package com.alesharik;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {
    final static String INET_ADDR = "224.0.0.4";
    final static int PORT = 8888;
    private static final byte TYPE_WHEEL_MANUAL = 123;

    public static final byte LEFT = 1;
    public static final byte RIGHT = 2;
    public static final byte TOP = 3;
    public static final byte BOTTOM = 4;
    private static final byte TYPE_STABIL_Z = 1;
    private static final byte TYPE_STABIL_Y = 2;
    private static final byte TYPE_STABIL_Z_BACK = 21;
    private static final byte TYPE_STABIL_Y_BACK = 22;
    private static final byte TYPE_SPEAK_COMMAND = 124;

    public static void main(String[] args) throws IOException, InterruptedException {
        MulticastSocket socket = new MulticastSocket(PORT);
        socket.joinGroup(InetAddress.getByName(INET_ADDR));
        Socket s = new Socket("localhost", 1200);
        DataOutputStream stream = new DataOutputStream(s.getOutputStream());
        new MsgThread(stream).start();
        while (true) {
            byte[] values = new byte[4 * 3 + 1];
            DatagramPacket packet = new DatagramPacket(values, values.length);
            socket.receive(packet);
            ByteBuffer byteBuffer = ByteBuffer.wrap(values);
            byte b = byteBuffer.get();
            if(b == 1) {
                float azimuth = byteBuffer.getFloat();
                float pitch = byteBuffer.getFloat();
                float roll = byteBuffer.getFloat();
                System.out.println("Azimuth: " + azimuth + ", pitch: " + pitch + ", roll: " + roll);

                int speed = (int) (roll * -1 / 180 * 100 * 2);
                int leftRot = speed;
                int rightRot = speed;

                leftRot += (speed >= 0 ? 1 : -1) * 0.5F * pitch;
                rightRot -= (speed >= 0 ? 1 : -1) * 0.5F * pitch;

                if (Math.abs(speed) < 8) {
                    leftRot = 0;
                    rightRot = 0;
                }

                System.out.println("Left: " + leftRot + ", right: " + rightRot);

                stream.writeByte(TYPE_WHEEL_MANUAL);
                stream.writeByte(leftRot);
                stream.writeByte(rightRot);
                stream.flush();
            } else if(b == 2) {
                byte type = byteBuffer.get();
                System.out.println("Type: " + type);
                if(type == BOTTOM)
                    stream.writeByte(TYPE_STABIL_Y_BACK);
                else if(type == TOP)
                    stream.writeByte(TYPE_STABIL_Y);
                else if(type == LEFT)
                    stream.writeByte(TYPE_STABIL_Z);
                else if(type == RIGHT)
                    stream.writeByte(TYPE_STABIL_Z_BACK);
                stream.flush();
            }
            s.getInputStream().read();
            Thread.sleep(25);
        }
    }

    private static final class MsgThread extends Thread {
        private final DataOutputStream stream;

        private MsgThread(DataOutputStream stream) {
            this.stream = stream;
            setDaemon(true);
        }

        @Override
        public void run() {
            Scanner scanner = new Scanner(System.in);
            while (!Thread.currentThread().isInterrupted()) {
                String s = scanner.nextLine();
                try {
                    stream.writeByte(TYPE_SPEAK_COMMAND);
                    byte[] bytes = s.getBytes(StandardCharsets.US_ASCII);
                    stream.writeByte(bytes.length);
                    for (int i = 0; i < Math.min(bytes.length, 120); i++) {
                        stream.writeByte(bytes[i]);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
